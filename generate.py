import sys
import jinja2
import datetime
from   os                     import walk
from   os                     import path
from   astral                 import LocationInfo
from   calendar               import monthrange
from   astral.sun             import sun
from   dateutil.relativedelta import relativedelta

loc = LocationInfo(timezone='Europe/Berlin', latitude=52.4937522, longitude=13.4481537)
iconpath = "https://icons.23-5.eu"

year  = datetime.date.today().year
month = datetime.date.today().month

if len(sys.argv) < 2:
    print("Missing parameter must be x or year month")
    sys.exit()

if sys.argv[1] == "x":
    y     = datetime.date.today() - relativedelta(days = 1)
    year  = y.year
    month = y.month
else:
    year  = int(sys.argv[1])
    month = int(sys.argv[2])

month_name = {}
month_name[ 1] = "January"
month_name[ 2] = "February"
month_name[ 3] = "March"
month_name[ 4] = "April"
month_name[ 5] = "May"
month_name[ 6] = "June"
month_name[ 7] = "July"
month_name[ 8] = "August"
month_name[ 9] = "September"
month_name[10] = "October"
month_name[11] = "November"
month_name[12] = "December"

vars = {}
vars['title_date'] = "%s - %s" % (year, month)
vars['year']       = year
vars['month']      = month
vars['month_name'] = month_name[month]
vars['iconpath']   = iconpath

today_dt = datetime.datetime(year, month, 1, 12, 12, 12)

links = {}
links['month_to_last_year']   = (today_dt - relativedelta(years  = 1)).strftime("%Y%m.html")
links['month_to_next_year']   = (today_dt + relativedelta(years  = 1)).strftime("%Y%m.html")
links['month_to_last_month']  = (today_dt - relativedelta(months = 1)).strftime("%Y%m.html")
links['month_to_next_month']  = (today_dt + relativedelta(months = 1)).strftime("%Y%m.html")

for (dirpath, dirnames, filenames) in walk("/host/Archived/"):
    break

dirnames.sort()
monthinfo = []

for dir in dirnames:
    d_raw = dir.split("_")[1]
    date_time_obj = datetime.datetime.strptime(d_raw, '%Y%m%d')
    
    if date_time_obj.year == year and date_time_obj.month == month: 
        print("/host/Archived/" + dir)
        day             = date_time_obj.day
        data            = {}
        data['number']  = day
        data['daylink'] = d_raw
        data['sunset']  = sun(loc.observer, date= datetime.date(year, month, day),                          tzinfo=loc.timezone)['sunset'] 
        data['sunrise'] = sun(loc.observer, date=(datetime.date(year, month, day) + relativedelta(days=1)), tzinfo=loc.timezone)['sunrise'] 

        if path.exists("/host/Archived/%s/FTPdetectinfo_%s.txt" %(dir, dir)):
            with open("/host/Archived/%s/FTPdetectinfo_%s.txt" %(dir, dir), 'r') as f:
                line = f.readline()
                data['meteor_count'] = int(line.split("=")[1])
        else:
            data['meteor_count'] = "no data"

        if path.exists("/host/Archived/%s/%s_stacked_with_stars.jpg" %(dir, dir)):
            data['stacked'] = "%s/%s_stacked_with_stars.jpg" % (dir, dir)
        else:
            data['stacked'] = "%s/not_interested-24px.svg" % iconpath

        if path.exists("/host/Archived/%s/%s_keogram.jpg" %(dir, dir)):
            data['keogram'] = "%s/%s_keogram.jpg" % (dir, dir)
        else:
            data['keogram'] = "%s/not_interested-24px.svg" % iconpath

        if path.exists("/host/Archived/%s/%s_fieldsums_noavg.png" %(dir, dir)):
            data['fieldsum_noavg'] = "%s/%s_fieldsums_noavg.png" % (dir, dir)
        else:
            data['fieldsum_noavg'] = "%s/not_interested-24px.svg" % iconpath

        if path.exists("/host/Archived/%s/%s_fieldsums.png" %(dir, dir)):
            data['fieldsum'] = "%s/%s_fieldsums.png" % (dir, dir)
        else:
            data['fieldsum'] = "%s/not_interested-24px.svg" % iconpath

        if path.exists("/host/Archived/%s/%s_CAPTURED_thumbs.jpg" %(dir, dir)):
            data['captured'] = "%s/%s_CAPTURED_thumbs.jpg" % (dir, dir)
        else:
            data['captured'] = "%s/not_interested-24px.svg" % iconpath

        if path.exists("/host/Archived/%s/%s_DETECTED_thumbs.jpg" %(dir, dir)):
            data['detected'] = "%s/%s_DETECTED_thumbs.jpg" % (dir, dir)
        else:
            data['detected'] = "%s/not_interested-24px.svg" % iconpath

        if path.exists("/host/Archived/%s/%s.mp4" %(dir, dir)):
            data['timelapse'] = "%s/%s.mp4" % (dir, dir)
        else:
            data['timelapse'] = "%s/not_interested-24px.svg" % iconpath




        monthinfo.append(data) 

templateLoader = jinja2.FileSystemLoader(searchpath="/app/templates")
templateEnv    = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE  = "month.html"
template       = templateEnv.get_template(TEMPLATE_FILE)
outputText     = template.render(vars=vars, links=links, monthinfo=monthinfo)

with open(today_dt.strftime("/host/Archived/%Y%m.html"), 'w') as the_file:
    the_file.write(outputText)
