#!/bin/bash

cd /app/RMS-master

echo "Create Timelaps"
for d in `find /host/Captured/ -type d | grep -v Fieldsums | grep DE` 
do

 echo "Process: ${d}"
 name=`echo ${d} | cut -d "/" -f 4`

 # first delete old temp folders
 if [[ ${d} == "*temp_img_dir*" ]]
 then
    rm -rf ${d}
    continue
 fi

 echo "Process Timelapse"
 count=`ls -1 /host/Archived/${name}/${name}.mp4 2>/dev/null | wc -l`
 if [ $count == 0 ]
 then
    python3 -m Utils.GenerateTimelapse ${d}
    mv /host/Captured/${name}/${name}.mp4 /host/Archived/${name}/
 fi

 echo "Process keogram"
 count=`ls -1 /host/Archived/${name}/${name}_keogram.jpg 2>/dev/null | wc -l`
 if [ $count == 0 ]
 then
  python3 -m Utils.BatchFFtoImage /host/Captured/${name}/ jpg
  mkdir /tmp/process
  mv /host/Captured/${name}/FF*.jpg /tmp/process/
  /app/keogram /tmp/process jpg /host/Archived/${name}/${name}_keogram.jpg
  rm -rf /tmp/process
 fi

done

echo "Stack images"
for d in `find /host/Archived/ -type d | grep DE` 
do

 echo "Stack with stars"
 count=`ls -1 ${d}/*stacked_with_stars.jpg 2>/dev/null | wc -l`
 if [ $count == 0 ]
 then
    name=`echo ${d} | cut -d "/" -f 4`
    python3 -m Utils.StackFFs ${d} jpg 
    mv ${d}/${name}_stack_1_meteors.jpg ${d}/${name}_stacked_with_stars.jpg
 fi

 echo "Stack without stars"
 count=`ls -1 ${d}/*stacked_without_stars.jpg 2>/dev/null | wc -l`
 if [ $count == 0 ]
 then
    name=`echo ${d} | cut -d "/" -f 4`
    python3 -m Utils.StackFFs ${d} jpg 
    mv ${d}/${name}_stack_1_meteors.jpg ${d}/${name}_stacked_without_stars.jpg
 fi

done

rm /host/Archived/*_detected.tar.bz2
