FROM ubuntu:22.04

ENV DEBIAN_FRONTEND="noninteractive" 

RUN  useradd -r -u 1001 appuser

# Basic stuff
RUN apt update     && \
    apt upgrade -y && \
    apt install -y curl mc vim python3 pip ffmpeg libopencv-dev libgl1-mesa-glx build-essential python3-tk

COPY requirements.txt requirements.txt
RUN  pip3 install -r  requirements.txt

RUN mkdir /app
RUN mkdir /home/appuser
RUN chown appuser /app
RUN chown appuser /home/appuser

WORKDIR /app
USER    appuser

ADD process.sh /app/process.sh

# RMS stuff
RUN curl -L https://github.com/CroatianMeteorNetwork/RMS/archive/master.zip > master.zip && \
    unzip master.zip && \
    rm master.zip

# Keogram stuff
RUN curl -L https://raw.githubusercontent.com/thomasjacquin/allsky/master/src/keogram.cpp > keogram.cpp
RUN g++ keogram.cpp -o keogram -Wall -Wno-psabi -g -D_LIN -D_DEBUG  -DOPENCV_C_HEADERS  -Llib/x64 -I./include -lpthread  -DGLIBC_20 -I/usr/include/opencv4 -lopencv_stitching -lopencv_alphamat -lopencv_aruco -lopencv_bgsegm -lopencv_bioinspired -lopencv_ccalib -lopencv_dnn_objdetect -lopencv_dnn_superres -lopencv_dpm -lopencv_face -lopencv_freetype -lopencv_fuzzy -lopencv_hdf -lopencv_hfs -lopencv_img_hash -lopencv_intensity_transform -lopencv_line_descriptor -lopencv_mcc -lopencv_quality -lopencv_rapid -lopencv_reg -lopencv_rgbd -lopencv_saliency -lopencv_shape -lopencv_stereo -lopencv_structured_light -lopencv_phase_unwrapping -lopencv_superres -lopencv_optflow -lopencv_surface_matching -lopencv_tracking -lopencv_highgui -lopencv_datasets -lopencv_text -lopencv_plot -lopencv_ml -lopencv_videostab -lopencv_videoio -lopencv_viz -lopencv_ximgproc -lopencv_video -lopencv_dnn -lopencv_xobjdetect -lopencv_objdetect -lopencv_calib3d -lopencv_imgcodecs -lopencv_features2d -lopencv_flann -lopencv_xphoto -lopencv_photo -lopencv_imgproc -lopencv_core

ADD generate.py /app/generate.py
ADD templates   /app/templates
